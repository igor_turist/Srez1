package innopolis.igor;

import java.util.HashSet;
import java.util.Random;

public class Main {

    private static volatile boolean finish = false;
    private static final int VALUE_COUNT = 5;
    private static final int SLEEP1 = 1000; //ms
    private static volatile int cnt = 0;
    private static volatile HashSet set = new HashSet(Main.VALUE_COUNT);

    public static void main(String[] args) {

	    Runnable r1 = new Runnable() {
            @Override
            public void run() {
                while(!finish){
                    Random r = new Random();
                    int val = r.nextInt(VALUE_COUNT);
                    synchronized (set) {
                        if (!set.contains(new Integer(val))) {
                            set.add(val);
                            System.out.println("new element found: " + val);
                        }
                        else System.out.println(val);

                        if (set.size() == VALUE_COUNT) {
                            finish = true;
                            set.notify();
                            System.out.println("Limit achieved!!!");
                        }
                        else {
                            if (cnt == 5) {
                                set.notify();
                                cnt = 0;
                            } else cnt++;
                        }

                        try {
                            Thread.sleep(Main.SLEEP1);
                        }
                        catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                System.out.println("Thread1 has finished");
            }
        };

        Runnable r2 = new Runnable() {
            @Override
            public void run() {
                while(!finish){
                    synchronized (set){
                        try {
                            if(!finish)set.wait();
                            System.out.println("\tUnique elements count: " + set.size());
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                System.out.println("Thread2 has finished");
            }
        };

        Thread th1 = new Thread(r1);
        Thread th2 = new Thread(r2);

        th1.start();
        th2.start();


        try {
            th1.join();
            th2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
